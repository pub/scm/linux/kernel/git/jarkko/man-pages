.\" Copyright (C) 2021 Intel Corporation
.\"
.\" %%%LICENSE_START(VERBATIM)
.\" Permission is granted to make and distribute verbatim copies of this
.\" manual provided the copyright notice and this permission notice are
.\" preserved on all copies.
.\"
.\" Permission is granted to copy and distribute modified versions of this
.\" manual under the conditions for verbatim copying, provided that the
.\" entire resulting derived work is distributed under the terms of a
.\" permission notice identical to this one.
.\"
.\" Since the Linux kernel and libraries are constantly changing, this
.\" manual page may be incorrect or out-of-date.  The author(s) assume no
.\" responsibility for errors or omissions, or for damages resulting from
.\" the use of the information contained herein.  The author(s) may not
.\" have taken the same level of care in the production of this manual,
.\" which is licensed free of charge, as they might when working
.\" professionally.
.\"
.\" Formatted or processed versions of this manual, if unaccompanied by
.\" the source, must acknowledge the copyright and authors of this work.
.\" %%%LICENSE_END
.\"
.TH SGX 7 2021\-02\-02 "Linux" "Linux Programmer's Manual"
.PP
sgx - overview of Software Guard eXtensions
.SH SYNOPSIS
.nf
.B #include <asm/sgx.h>
.PP
.IB enclave " = open(" \(dq/dev/sgx_enclave\(dq ", O_RDWR);"
.fi
.SH DESCRIPTION
Intel Software Guard eXtensions (SGX) allow applications to host
enclaves,
protected executable objects in encrypted memory.
.PP
Enclaves are blobs of executable code,
running inside a CPU enforced container,
which is mapped to the process address space.
They are represented as the open files of
.IR /dev/sgx_enclave .
Enclaves can have an arbitrary fixed set of entry points,
entered with the leaf functions of ENCLU x86 opcode.
Once entered,
CPU executes inside the enclave and can access its memory.
Exceptions,
some x86 opcodes,
and privilege level changes will throw the CPU out of the enclave,
and they must be handled by the host.
.PP
SGX can only be available if the kernel is configured and built with the
.B CONFIG_X86_SGX
option.
If CPU, BIOS and kernel have SGX enabled,
.I sgx
appears in the
.I flags
field of
.IR /proc/cpuinfo .
.PP
If SGX appears not to be available,
ensure that SGX is enabled in the BIOS.
If a BIOS presents a choice between
.I Enabled
and
.I Software Enabled
modes for SGX,
choose
.IR Enabled .
.SS Memory mapping
The file descriptor of an enclave can be shared among multiple processes.
An enclave is required by the CPU to be placed to an address,
which is a multiple of its size.
An address range containing a reasonable base address can be probed with an anonymous
.BR mmap(2)
call:
.PP
.in +4
.EX
void *area = mmap(NULL, size * 2, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS,
                  -1, 0);

void *base = ((uintptr_t) area + size - 1) & ~(size - 1);
.EE
.in
.PP
The enclave file descriptor itself can be then mapped with the
.B MAP_FIXED
flag set to the carved out memory.
.SS Entering enclave
Thread control structure (TCS) pages are the entry points to the enclave,
which further define a byte offset inside the enclave where the execution begins.
Kernel defines
.IR __vdso_sgx_enter_enclave
.BR vdso (7)
symbol
for invoking TCS's.
Further, the prototype for this
.BR vdso (7)
is defined by
.I vdso_sgx_enter_enclave_t
in
.IR <asm/sgx.h> .
.SS Permissions
.PP
During the build process each enclave page is assigned protection bits,
as part of
.I SGX_IOC_ENCLAVE_ADD_PAGES .
These protections are also the maximum protections with which the page can be be mapped.
If
.BR mmap (2)
is called with higher protections than those defined during the build,
it will return
.B -EACCES .
If
.I SGX_IOC_ENCLAVE_ADD_PAGES
is called after
.BR mmap (2)
with lower protections,
the caller receives
.BR SIGBUS ,
once it accesses the page for the first time.
.SS Ioctls
An enclave instance is created by opening
.IR /dev/sgx_enclave .
Its contents are populated with the
.BR ioctl (2)
interface in
.IR <asm/sgx.h> :
.TP
.B SGX_IOC_ENCLAVE_CREATE
Create SGX Enclave Control Structure (SECS) for the enclave.
SECS is a hardware defined structure,
which contains the global properties of an enclave.
.B SGX_IOC_ENCLAVE_CREATE
is a one-shot call that fixes the enclave's address and
size for the rest of its life-cycle.
.TP
.B SGX_IOC_ENCLAVE_ADD_PAGES
Fill a range of the enclave's pages with the caller provided data and protection bits.
Memory mappings of the enclave can only set protection bits that are defined in this ioctl.
The pages added are either regular pages for code and data,
or thread control structures (TCS).
The latter define the entry points to the enclave,
which can be entered after the initialization.
.TP
.B SGX_IOC_ENCLAVE_INIT
Initialize the enclave for the run-time.
After a successful initialization,
no new pages can be added to the enclave.
.SH VERSIONS
The SGX feature was added in Linux 5.11.
.SH SEE ALSO
.BR ioctl (2),
.BR mmap (2),
.BR mprotect (2)
.BR vdso (7)
